﻿using UnityEngine;
using System.Collections;

public class Sphere : MonoBehaviour {

    // final time
    private float futureTime = 0;

    // delta time
    private int countDown = 0;

    // time after that sphere destroied ( in seconds )
    private float sphereDuration = 8;

	// Use this for initialization
	void Start () {
        // set up the final time
        futureTime = sphereDuration + Time.realtimeSinceStartup;
	}
	
	// Update is called once per frame
	void Update () {
        // actual time
        float rightNow = Time.realtimeSinceStartup;

        // new delta time
        countDown = (int)futureTime - (int)rightNow;

        // if count down less than 0 destroy sphere object
        if (countDown < 0) Destroy(gameObject);
	}
}
